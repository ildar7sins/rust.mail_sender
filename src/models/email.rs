use std::fmt;
use validator::Validate;
use serde::{Serialize, Deserialize};

#[derive(Debug, Deserialize, Serialize, Clone, Validate)]
pub struct NewEmail {
	#[validate(required)]
	#[validate(email)]
	pub to: Option<String>,
	#[validate(required)]
	#[validate(email)]
	pub from: Option<String>,
	#[validate(email)]
	pub reply_to: Option<String>,
	#[validate(required)]
	pub subject: Option<String>,
	#[validate(required)]
	pub body: Option<String>,
}

impl fmt::Display for NewEmail {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(
			f,
			"to: {:?}, from: {:?}, reply_to: {:?}, subject: {:?}, body: {:?}",
			self.to, self.from, self.reply_to, self.subject, self.subject,
		)
	}
}


#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Email {
	pub to: String,
	pub from: String,
	pub reply_to: Option<String>,
	pub subject: String,
	pub body: String,
}

impl From<NewEmail> for Email {
	fn from(email: NewEmail) -> Self {
		Self {
			to: email.to.unwrap(),
			from: email.from.unwrap(),
			reply_to: email.reply_to,
			subject: email.subject.unwrap(),
			body: email.body.unwrap(),
		}
	}
}
