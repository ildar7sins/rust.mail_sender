use deadpool_lapin::PoolError;
use thiserror::Error as ThisError;
use validator::ValidationErrors;

#[derive(ThisError, Debug)]
pub enum Error {
	#[error("rmq error: {0}")]
	RMQError(#[from] lapin::Error),

	#[error("rmq pool error: {0}")]
	RMQPoolError(#[from] PoolError),

	#[error("validation error: {0}")]
	Validation(ValidationErrors),
}

impl warp::reject::Reject for Error {}

