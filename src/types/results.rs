use warp::Rejection;
use deadpool_lapin::PoolError;
use crate::types::errors::Error;
use std::result::Result as StdResult;

pub type Result<T> = StdResult<T, Error>;

pub type WebResult<T> = StdResult<T, Rejection>;
pub type RMQResult<T> = StdResult<T, PoolError>;
