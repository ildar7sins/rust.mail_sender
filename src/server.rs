use validator::Validate;
use std::convert::Infallible;
use crate::types::errors::Error;
use serde::de::DeserializeOwned;
use crate::models::email::NewEmail;
use warp::{Filter, Rejection};
use crate::rmq_listener::{Pool};

pub fn json_to_new_email() -> impl Filter<Extract = (NewEmail,), Error = Rejection> + Clone
{
	warp::body::content_length_limit(1024 * 16).and(warp::body::json())
}

pub fn validate<T>(value: T) -> Result<T, Error> where T: Validate
{
	value.validate().map_err(Error::Validation)?;

	Ok(value)
}

pub fn with_validated_json<T>() -> impl Filter<Extract = (T,), Error = Rejection> + Clone
	where
		T: DeserializeOwned + Validate + Send,
{
	warp::body::content_length_limit(1024 * 16)
		.and(warp::body::json())
		.and_then(|value| async move { validate(value).map_err(warp::reject::custom) })
}

// Пока закоментировал обработку ошибок из функции...
// pub async fn handle_rejection(err: Rejection) -> Result<impl Reply, Infallible> {
// 	let response = if let Some(e) = err.find::<Error>() {
// 		handle_crate_error(e)
// 	} else {
// 		HttpApiProblem::with_title_and_type_from_status(StatusCode::INTERNAL_SERVER_ERROR)
// 	};
//
// 	Ok(response.to_hyper_response())
// }
//
// pub fn handle_crate_error(e: &Error) -> HttpApiProblem {
// 	match e {
// 		Error::Validation(errors) => {
// 			let mut problem =
// 				HttpApiProblem::with_title_and_type_from_status(StatusCode::BAD_REQUEST)
// 					.set_title("One or more validation errors occurred")
// 					.set_detail("Please refer to the errors property for additional details");
//
// 			let _ = problem.set_value("errors", errors.errors());
//
// 			problem
// 		}
// 		_ => HttpApiProblem::with_title_and_type_from_status(StatusCode::BAD_REQUEST)
// 			.set_title("Server error occurred")
// 			.set_detail("Please contact the administrator")
// 	}
// }

pub fn with_rmq(pool: Pool) -> impl Filter<Extract = (Pool,), Error = Infallible> + Clone {
	warp::any().map(move || pool.clone())
}
