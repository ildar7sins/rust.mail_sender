use dotenv;
use std::str;
use tokio_amqp::*;
use std::time::Duration;
use deadpool_lapin::{Manager};
use crate::types::results::{Result, RMQResult};
use crate::mail::sender;
use lapin::{options::*, types::FieldTable, ConnectionProperties};
use futures::StreamExt;
use crate::models::email::{Email};

pub type Pool = deadpool::managed::Pool<lapin::Connection, lapin::Error>;
pub type Connection = deadpool::managed::Object<lapin::Connection, lapin::Error>;

pub fn get_pool() -> Pool {
	let addr: String = dotenv::var("RMQ_DSN").unwrap_or_else(|_| "amqp://rmq:rmq@localhost:5672".into());

	let manager = Manager::new(addr, ConnectionProperties::default().with_tokio());
	deadpool::managed::Pool::new(manager, 10)
}

pub async fn get_rmq_con(pool: Pool) -> RMQResult<Connection> {
	let connection = pool.get().await?;
	Ok(connection)
}

pub async fn rmq_listen(pool: Pool) -> Result<()> {
	let mut retry_interval = tokio::time::interval(Duration::from_secs(5));

	loop {
		retry_interval.tick().await;
		println!("connecting rmq consumer...");
		match init_rmq_listen(pool.clone()).await {
			Ok(_) => println!("rmq listen returned"),
			Err(e) => eprintln!("rmq listen had an error: {:?}", e),
		};
	}
}

async fn init_rmq_listen(pool: Pool) -> Result<()> {
	let rmq_con = get_rmq_con(pool).await.map_err(|e| {
		eprintln!("could not get rmq con: {}", e);
		e
	})?;
	let channel = rmq_con.create_channel().await?;

	let _queue = channel
		.queue_declare(
			"hello",
			QueueDeclareOptions::default(),
			FieldTable::default(),
		)
		.await?;
	// println!("Declared queue {:?}", queue);

	let mut consumer = channel
		.basic_consume(
			"hello",
			"my_consumer",
			BasicConsumeOptions::default(),
			FieldTable::default(),
		)
		.await?;

	// println!("rmq consumer connected, waiting for messages");
	while let Some(delivery) = consumer.next().await {
		if let Ok((channel, delivery)) = delivery {
			let data: &str = str::from_utf8(&delivery.data).unwrap();
			let email: Email = serde_json::from_str(data).unwrap();

			// Send the email...
			match sender::send_email(email) {
				Ok(_) => println!("Email sent successfully!"),
				Err(e) => panic!("Could not send email: {:?}", e),
			}

			channel
				.basic_ack(delivery.delivery_tag, BasicAckOptions::default())
				.await?
		}
	}
	Ok(())
}
