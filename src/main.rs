use warp::{Filter};
use futures::join;
use server::{json_to_new_email};
use types::results::Result;
use rmq_listener::{Pool, rmq_listen, get_pool};

// Api handlers...
use crate::api_handlers::test::{test_handler, more_test};
use crate::api_handlers::base::health_handler;
use crate::api_handlers::messages::{add_msg_handler};

use crate::server::{with_validated_json, with_rmq};

mod types;
mod models;
mod server;
mod mail;
mod rmq_listener;
mod api_handlers;

#[tokio::main]
async fn main() -> Result<()> {
    dotenv::dotenv().ok();

    let pool: Pool = get_pool();

    let health_route = warp::path!("health").and_then(health_handler);

    let add_msg_route = warp::path!("msg")
        .and(warp::post())
        .and(with_validated_json())
        .and(with_rmq(pool.clone()))
        .and_then(add_msg_handler);

    let test = warp::path!("test")
        .and(warp::post())
        .and(json_to_new_email())
        .and(with_rmq(pool.clone()))
        .and_then(test_handler);

    let hello = warp::path!("hello")
        .and(warp::post())
        .and(with_validated_json())
        .and_then(more_test);
        // .recover(handle_rejection);

    let routes = health_route
        .or(add_msg_route)
        .or(test)
        .or(hello)
        ;

    println!("Started server at localhost:8000");

    let _ = join!(
        warp::serve(routes).run(([0, 0, 0, 0], 8000)),
        rmq_listen(pool.clone())
    );

    Ok(())
}



