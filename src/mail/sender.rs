use lettre::transport::smtp::authentication::Credentials;
use lettre::{Message, SmtpTransport, Transport};
use lettre::transport::smtp::response::Response;
use lettre::transport::smtp::Error;
use crate::models::email::Email;
use lettre::message::{MultiPart, SinglePart};

pub fn send_email(email: Email) -> Result<Response, Error> {


	let email = Message::builder()
		.from("office@mbrin.ru".parse().unwrap())
		.reply_to("Yuin <office@mbrin.ru>".parse().unwrap())
		.to(email.to.parse().unwrap())
		.subject("Happy new year")
		// .body(email.body)
		// .multipart(MultiPart::related().singlepart(SinglePart::html(email.body)))
		.singlepart(SinglePart::html(email.body))
		.unwrap();


	let creds = Credentials::new("office@mbrin.ru".to_string(), "10125117No".to_string());

	// Open a remote connection to gmail
	let mailer = SmtpTransport::relay("smtp.yandex.ru")
		.unwrap()
		.credentials(creds)
		.build();

	mailer.send(&email)
}
