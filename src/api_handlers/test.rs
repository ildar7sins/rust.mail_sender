use crate::models::email::NewEmail;
use crate::types::errors::Error;
use crate::rmq_listener::{Pool, get_rmq_con};
use warp::Reply;
use lapin::{options::BasicPublishOptions, BasicProperties};
use crate::types::results::WebResult;

pub async fn test_handler(email: NewEmail, pool: Pool) -> WebResult<impl Reply> {

	let email_string = serde_json::to_string(&email).unwrap_or("{}".to_string());
	let payload = email_string.as_bytes();

	let rmq_con = get_rmq_con(pool).await.map_err(|e| {
		eprintln!("can't connect to rmq, {}", e);
		warp::reject::custom(Error::RMQPoolError(e))
	})?;

	let channel = rmq_con.create_channel().await.map_err(|e| {
		eprintln!("can't create channel, {}", e);
		warp::reject::custom(Error::RMQError(e))
	})?;

	channel
		.basic_publish(
			"",
			"hello",
			BasicPublishOptions::default(),
			payload.to_vec(),
			BasicProperties::default(),
		)
		.await
		.map_err(|e| {
			eprintln!("can't publish: {}", e);
			warp::reject::custom(Error::RMQError(e))
		})?
		.await
		.map_err(|e| {
			eprintln!("can't publish: {}", e);
			warp::reject::custom(Error::RMQError(e))
		})?;

	Ok("OK BOY")
}

pub async fn more_test(email: NewEmail) -> WebResult<impl Reply> {

	Ok(format!("Hello, Ildar! This is request: {:?}", email))
}
