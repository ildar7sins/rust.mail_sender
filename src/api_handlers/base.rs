use warp::Reply;
use crate::types::results::WebResult;

pub async fn health_handler() -> WebResult<impl Reply> {
	Ok("OK")
}
