use warp::{Reply};
use crate::types::errors::Error;
use crate::rmq_listener::{Pool, get_rmq_con};
use lapin::{options::BasicPublishOptions, BasicProperties};
use crate::types::results::WebResult;
use crate::models::email::{NewEmail, Email};

pub async fn add_msg_handler(new_email: NewEmail, pool: Pool) -> WebResult<impl Reply> {
	let email: Email = new_email.into();
	let email_string = serde_json::to_string(&email).unwrap_or("{}".to_string());
	let payload = email_string.as_bytes();

	let rmq_con = get_rmq_con(pool).await.map_err(|e| {
		eprintln!("can't connect to rmq, {}", e);
		warp::reject::custom(Error::RMQPoolError(e))
	})?;

	let channel = rmq_con.create_channel().await.map_err(|e| {
		eprintln!("can't create channel, {}", e);
		warp::reject::custom(Error::RMQError(e))
	})?;


	channel
		.basic_publish(
			"",
			"hello",
			BasicPublishOptions::default(),
			payload.to_vec(),
			BasicProperties::default(),
		)
		.await
		.map_err(|e| {
			eprintln!("can't publish: {}", e);
			warp::reject::custom(Error::RMQError(e))
		})?
		.await
		.map_err(|e| {
			eprintln!("can't publish: {}", e);
			warp::reject::custom(Error::RMQError(e))
		})?;
	Ok("OK")
}
